class Hash

  def bubble_sort(by_key=true)
    sort_arr = self.to_a
    sort_idx = 0
    sort_idx = 1 unless by_key

    (sort_arr.length).times do |i|
      (sort_arr.length-i-1).times do |j|
        if sort_arr[j][sort_idx] > sort_arr[j+1][sort_idx]
          sort_arr[j+1],sort_arr[j] = sort_arr[j], sort_arr[j+1]
        end
      end
    end
    sort_arr
  end
end