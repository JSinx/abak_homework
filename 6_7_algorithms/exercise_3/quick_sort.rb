class Hash
  def quick_sort(by_key=true)
    arr_sort = self.to_a
    if by_key
      return quick_sort_array(arr_sort, 0)
    end
    quick_sort_array(arr_sort, 1)
  end

  private
    def quick_sort_array(sort_arr, sort_idx)
      len = sort_arr.length

      if len <= 1
        return sort_arr
      elsif len == 2
        if sort_arr[0][sort_idx] > sort_arr[1][sort_idx]
          #sort_arr[0], sort_arr[1] = sort_arr[1], sort_arr[0]
          return sort_arr[1], sort_arr[0]
        end
        return sort_arr
      end
      idx = 0
      left_idx, right_idx = 1, sort_arr.length-1

      #medium_val = ( sort_arr.first[sort_idx] + sort_arr[len / 2][sort_idx] + sort_arr.last[sort_idx] ) / 3
      medium_val = sort_arr[0][sort_idx]

      while left_idx <= right_idx do
        if sort_arr[left_idx][sort_idx] < medium_val
          left_idx += 1
        else
          if sort_arr[right_idx][sort_idx] >= medium_val
            right_idx -= 1
          else
            sort_arr[left_idx],sort_arr[right_idx] = sort_arr[right_idx], sort_arr[left_idx]
          end
        end
      end

      part_left = sort_arr[1...left_idx]
      part_right = sort_arr[left_idx...sort_arr.length]

      quick_sort_array(part_left, sort_idx) + [ sort_arr[0] ] + quick_sort_array(part_right, sort_idx)
    end

end