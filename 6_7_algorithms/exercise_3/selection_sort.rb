class Hash

  def selection_sort(by_key=true)
    sort_arr = self.to_a
    sort_idx = 0
    sort_idx = 1 unless by_key

    (sort_arr.length).times do |i|
      idx = 0

      (sort_arr.length-i-1).times do |j|
        if sort_arr[idx][sort_idx] < sort_arr[j+1][sort_idx]
          idx = j + 1
        end
      end

      if idx < sort_arr.length-i-1
        sort_arr[sort_arr.length-i-1],sort_arr[idx] = sort_arr[idx], sort_arr[sort_arr.length-i-1]
      end

    end
    sort_arr
  end
end