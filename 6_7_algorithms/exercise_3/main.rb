# Практическое задание No3
# Создать не отсортированный хеш с произвольными ключами и значениями,
# применить к нему алгоритмы сортировки.

require 'benchmark'
include Benchmark

require_relative('bubble_sort.rb')
require_relative('quick_sort.rb')
require_relative('quick_sort_2.rb')
require_relative('selection_sort.rb')

testing_hash = Hash.new{}
max_times = 1000

max_times.times do |i|
  testing_hash[rand(max_times*2)] = rand(max_times*2)
end

#max_times.times do |i|
#  testing_hash[max_times-i+1] = rand(max_times*2)
#end


testing_hash2 = {
 18 => 1,
 17 => 1,
  16 => 1,
  15 => 1,
  14 => 1,
  13 => 1,
  12 => 1,
  11 => 1,
  10 => 1,
   9 => 1,
   8 => 1,
   7 => 1,
   6 => 1,
  5 => 1,
  4 => 1,
  3 => 1,
  2 => 4,
  1 => 3,
  0 => 2,
}


#p testing_hash.to_a
p testing_hash2.sort
#p testing_hash.bubble_sort
p testing_hash2.quick_sort
p testing_hash2.quick_sort2
#p testing_hash.selection_sort


aaa = testing_hash.to_a
bbb = 0

Benchmark.benchmark(CAPTION, 7, FORMAT ) do |x|

#  x.report('Пузырьком   :')  { 100.times{ testing_hash.bubble_sort } }
#  x.report('Быстрая     :')  { 1000.times{ testing_hash.quick_sort }}
  x.report('Быстрая 2   :')  { 5000.times{ testing_hash.quick_sort2 }}
  x.report('Стандартная :')  { 5000.times{ testing_hash.sort }}
#  x.report('Выбором     :')  { 100.times{ testing_hash.selection_sort }}
end

p testing_hash.sort
p testing_hash.quick_sort2
# puts Benchmark.measure{ 100.times{ testing_hash.sort }}
# puts Benchmark.measure{ 100.times{ testing_hash.bubble_sort }}
# puts Benchmark.measure{ 100.times{ testing_hash.quick_sort }}
