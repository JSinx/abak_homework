class Hash
  def quick_sort2(by_key=true)
    arr_sort = self.to_a
    if by_key
      return quick_sort_array_2(arr_sort, 0)
    end
    quick_sort_array_2(arr_sort, 1)
  end

  private
    def quick_sort_array_2(sort_arr, sort_idx)
      len = sort_arr.length


      #if sort_arr.length <= 1
      if len <= 1
        return sort_arr
      elsif len == 2
        if sort_arr[0][sort_idx] > sort_arr[1][sort_idx]
          return [sort_arr[1], sort_arr[0]]
        end
        return sort_arr
      end

      sort_arr[0], sort_arr[len/2] = sort_arr[len/2], sort_arr[0]

      medium_val = sort_arr[0][sort_idx]

      part_left = []
      part_right = []

      while len > 1
        len -= 1
        if sort_arr[len][sort_idx] < medium_val
           part_left.push( sort_arr[len] )
        else
           part_right.push( sort_arr[len] )
        end
      end

      quick_sort_array_2(part_left, sort_idx) + [sort_arr[0]] + quick_sort_array_2(part_right, sort_idx)
#      quick_sort_array_2(part_left, sort_idx) + quick_sort_array_2(part_right, sort_idx)
    end
end