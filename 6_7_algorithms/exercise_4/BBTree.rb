class BBTree
  attr_reader :b_array

  attr_accessor :current_node

  def initialize(start_arr=nil)
    unless start_arr
      @b_array = []
      return
    end

    @b_array = start_arr
    @current_node = 0
    @where_i_was = []
  end

  def tree_walk_start(search_type = :direct)
    @current_node = 0
    @search_type = search_type
    @b_array[@current_node]
  end



  def tree_walk_next
    if @search_type == :symmetrical
      idx = check_node_symmetrical(@current_node)
    elsif @search_type == :reverse
      idx = check_node_reverse(@current_node)
    else
      idx = check_node_direct(@current_node)
    end


    if idx
      @where_i_was << idx
      @current_node = idx
      @b_array[idx]
    end
  end


  def parent(idx=nil)
#    return unless idx
    idx ||= @current_node
    if idx == 0
      return nil
    end
    (idx-1) / 2
  end

  def left(idx=nil)
#    return unless idx
    idx ||= @current_node
    tmp_left = idx*2+1
    if tmp_left > @b_array.length
      return nil
    end
    tmp_left
  end

  def right(idx=nil)
#    return unless idx
    idx ||= @current_node
    tmp_right = idx*2+2
    if tmp_right > @b_array.length
      return nil
    end
    tmp_right
  end

  def level(idx=nil)
    idx ||= @current_node
    Math.log2(idx+1).truncate
  end

  def left_sibling(idx=nil)
    idx ||= @current_node
    start_pos, end_pos = calc_level_pos(level(idx))
    if idx - 1 < start_pos
      return nil
    end
    idx - 1
  end

  def right_sibling(idx=nil)
    idx ||= @current_node
    start_pos, end_pos = calc_level_pos(level(idx))
    if idx + 1 > end_pos
      return nil
    end
    idx+1
  end

  def get_level(level_num)
    start_pos, end_pos = calc_level_pos(level_num)
    @b_array[start_pos...end_pos]
  end

  private

    def calc_level_pos(level_num)
      start_pos = 2**level_num-1
      end_pos = start_pos + 2**level_num

      return start_pos, end_pos
    end


    def check_node_direct(idx)
      return unless idx

      return idx if not_was_in_node(idx)
      return left(idx) if not_was_in_node(left(idx))
      return right(idx) if not_was_in_node(right(idx))

      check_node_direct(parent(idx))
    end

    def not_was_in_node(idx)
      return unless idx
      return unless @b_array[idx]

      @where_i_was.index(idx) == nil
    end


    def check_node_symmetrical(idx)
      return unless idx

      if not_was_in_node(idx)
        if not_was_in_node(left(idx))
          return check_node_symmetrical(left(idx))
        else
          return idx
        end
      end

      if not_was_in_node(right(idx))
        return check_node_symmetrical(right(idx))
      end

      check_node_symmetrical(parent(idx))
    end


    def check_node_reverse(idx)
      return unless idx

      if not_was_in_node(idx)
        if not_was_in_node(left(idx))
          return check_node_reverse(left(idx))
        elsif not_was_in_node(right(idx))
          return check_node_reverse(right(idx))
        else
          return idx
        end
      end
      check_node_reverse(parent(idx))
    end
end