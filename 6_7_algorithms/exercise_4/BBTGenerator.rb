class BBTGenerator


  def initialize(start_arr=nil)
    return unless start_arr
    btree_array = middle_search(start_arr.compact.sort)
    BBTree.new( btree_array )
  end


  def generate_btree(node_count)
    max_rand = node_count*3
    gen_arr = []

    node_count.times do
      begin
        gen_v = rand(max_rand)
      end while gen_arr.index(gen_v)
      gen_arr << gen_v
    end
    BBTree.new( middle_search(gen_arr.sort))
  end

  def middle_search(search_arr)
    search_arr.compact!

    if search_arr.length == 0
      return [nil]
    elsif search_arr.length == 1
      return search_arr
    elsif search_arr.length % 2 == 0
      search_arr << nil
    end

    middle_pos = search_arr.length / 2
    left_part = middle_search(search_arr[0...middle_pos])
    right_part = middle_search(search_arr[(middle_pos+1)...search_arr.length])

    left_part, right_part = align_arrays(left_part, right_part)

    [search_arr[middle_pos]]+stick_array(left_part, right_part)
  end

  def stick_array(array_1, array_2)
    i = 0
    result = []
    while true
      start_pos = 2**i-1
      if start_pos >= array_1.length
        break
      end
      end_pos = start_pos + 2**i

      result.concat(array_1[start_pos...end_pos])
      result.concat(array_2[start_pos...end_pos])

      i += 1
    end
    result
  end

  def align_arrays(array_1, array_2)
    length_diff = array_1.length - array_2.length

    if length_diff > 0
      length_diff.times { array_2 << nil }
    elsif length_diff < 0
      length_diff.abs.times  { array_1 << nil }
    end

    return array_1, array_2
  end
end