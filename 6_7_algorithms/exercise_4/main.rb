# Практическое задание No4
# Построение и обход идеально сбалансированных двоичных деревьев.
#
# Реализовать программу, выполняющую следующий набор операций:
# 1. построение идеально сбалансированного двоичного дерева с заданным числом вершин;
# 2. построчный вывод дерева на основе процедуры обхода в прямом порядке;
# 3. построчный вывод дерева на основе процедуры обхода в симметричном порядке;
# 4. построчный вывод дерева на основе процедуры обхода в обратном порядке.


require_relative('BBTree')
require_relative('BBTGenerator')

testing_hash = Hash.new{}
max_times = 100

max_times.times do |i|
  testing_hash[rand(max_times*2)] = rand(max_times*2)
end

testing_hash2 = {
    1 => 0,
    3 => 0,
    5 => 0,
    7 => 0,
    11 => 0,
    13 => 0,
    15 => 0,
    17 => 0,
}

#testing_arr = [1,3,5,7,11,13,15,17]
#testing_arr_bb = [10,8,20,4,9,6,23,3,5,nil,nil,nil,nil,21,nil,nil,nil,2,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]

bgen = BBTGenerator.new()

test_b = bgen.generate_btree(50)
i = 0
begin
  lv = test_b.get_level(i)
  p lv
  i += 1
end while lv


test_b.tree_walk_start(:direct)
puts '--------------------'
begin
  vv = test_b.tree_walk_next
  puts vv
end while vv


