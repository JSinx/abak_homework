class DataFinder < Array

  def sort_by_key(key)
    DataFinder.new(self.sort{ |obj1, obj2| obj1[key] <=> obj2[key] })
  end

  def sort_by_key!(key)
    self.sort!{ |obj1, obj2| obj1[key] <=> obj2[key] }
  end


  def find_slow_interval(key, min_value, min_equal=true, max_value=nil, max_equal=true)
    result = DataFinder.new

    self.each_entry do |value|
      if (min_equal || (not max_value ))  && value[key] == min_value
        result <<  value
      elsif max_value
        if max_equal && value[key] == max_value
          result <<  value
        elsif value[key] > min_value && value[key] < max_value
          result <<  value
        end
      # elsif value[key] > min_value
      #   result <<  value
      end
    end
    result
  end

  def find_slow_exact_value(key, value)
    result = DataFinder.new
    self.each_entry do |item|
      if item[key] == value
        result << item
      end
    end
    result
  end


  def find_interval(key, min_value, min_equal=true, max_value=nil, max_equal=true)
    result = DataFinder.new

    if min_value && max_value
      pos_start, pos_end = BSearch.find_interval(self, key, min_value, min_equal, max_value, max_equal)
    elsif min_value && (not max_value)
      pos_start, pos_end = BSearch.find_interval(self, key, min_value, true, min_value, true)
    else
      return self
    end
    result.concat( self[pos_start, pos_end] )
  end

end