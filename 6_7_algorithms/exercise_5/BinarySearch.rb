class BSearch

  def self.find_in_array(my_array, key, value)
    return if my_array.length <= 0

    middle_pos = my_array.length / 2

    if my_array[middle_pos][key] == value
      return middle_pos
    elsif my_array[middle_pos][key] > value
      return find_in_array(my_array[0...middle_pos], key, value)
    else
      find_in_array(my_array[middle_pos+1...my_array.length], key, value)+middle_pos+1
    end
  end


  def self.find_in_array_bigger(my_array, key, value, equal = true)
    return if my_array.length <= 0

    middle_pos = my_array.length / 2

    if equal && (my_array[middle_pos][key] == value)
#      while (middle_pos > 0) && (my_array[middle_pos-1][key] == value)
#        middle_pos -= 1
#      end
      pos = find_in_array_bigger(my_array[0...middle_pos], key, value, true)
      return pos || middle_pos
    elsif my_array[middle_pos][key] > value
      pos = find_in_array_bigger(my_array[0...middle_pos], key, value, equal)
      return pos || middle_pos
    else
      pos = find_in_array_bigger(my_array[middle_pos+1...my_array.length], key, value, equal)
      return ( pos + middle_pos + 1 ) if pos
    end
  end


  def self.find_in_array_smaller(my_array, key, value, equal = true)
    return if my_array.length <= 0

    middle_pos = my_array.length / 2

    if equal && (my_array[middle_pos][key] == value)
      # while (middle_pos < my_array.length) && (my_array[middle_pos+1][key] == value)
      #   middle_pos += 1
      # end
      pos = find_in_array_smaller(my_array[middle_pos+1...my_array.length], key, value, true)
      return ( pos + middle_pos + 1 ) if pos
      middle_pos
    elsif my_array[middle_pos][key] < value
      pos = find_in_array_smaller(my_array[middle_pos+1...my_array.length], key, value, equal)
      return ( pos + middle_pos + 1 ) if pos
      middle_pos
    else
      pos = find_in_array_smaller(my_array[0...middle_pos], key, value, equal)
      return ( pos ) if pos
    end
  end

  def self.find_interval(my_array, key, min_value, min_equal=true, max_value=nil, max_equal=true)
    min_pos = find_in_array_bigger(my_array, key, min_value, min_equal )
    max_pos = nil || ( find_in_array_smaller(my_array, key, max_value, max_equal) if max_value )

    return min_pos, max_pos
  end
end