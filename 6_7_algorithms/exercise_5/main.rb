# Практическое задание No5
#
# В памяти есть 10 000 000 объектов со следующими полями:
#
#  1. возраст (0..100)
# 2. зарплата (0..1000000,0)
# 3. рост (0..200)
# 4. вес (0..200)
#
# Нужно написать максимально быстрый алгоритм для выбора объектов по определённым условиям
# (условий может быть от 0 до 4, в качестве условия можно указать диапазон значений).

require_relative('BinarySearch')
require_relative('DataFind')

test_arr = DataFinder.new

#            0 1 2 3 4  5  6  7  8  9  10 11 12 13 14
test_ages = [1,2,5,7,8,10,10,10,19,20,23,25,30,40,50]

1500.times do |i|
  new_obj = {}
#  new_obj[:age] = test_ages[i]
  new_obj[:age] = rand(100)
  new_obj[:salary] = rand(1000000)
  new_obj[:height] = rand(200)
  new_obj[:weight] = rand(200)
  test_arr << new_obj
end


#p test_arr

#p test_arr.sort! { |obj1, obj2| obj1[:age] <=> obj2[:age] }

# 10.times do |i|
#   value = test_arr[i][:age]
#   puts "#{i}:  #{BSearch.find_in_array(test_arr, :age, value)}"
# end


#value = 10
#puts BSearch.find_in_array_bigger(test_arr, :age, value, false)
#puts BSearch.find_in_array_smaller(test_arr, :age, 20, true)

#p BSearch.find_interval(test_arr, :age, 10, false, 20,  true)
#p BSearch.find_interval(test_arr, :age, 10, true, 10)


#puts test_arr.find_slow_interval(:age, 10, false, 15)

ages_data = test_arr.find_slow_interval(:age, 18, true, 25, true)
p ages_data.length

height_data = ages_data.sort_by_key(:height).find_interval(:height, 170, true, 180, true)
p height_data.length

weight_data = height_data.sort_by_key(:weight).find_interval(:weight, 75, true, 90, true)
p weight_data.length

p weight_data

#p height_data
#p weight_data