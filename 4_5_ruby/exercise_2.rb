# Существует точка на карте с координатами x и y (например, x = 60.597223, y =  56.837992),
# эта точка находится в центре окружности, радиусом в r. Необходимо написать программу на
# ruby, на входе которой будет массив из N точек
# [[x0, y0], [x1, x2], …], а на выходе массив содержащий точки, которые входят в
# данную область

CONST_X = 0
CONST_Y = 0

def calc_distance (x1,y1, x2, y2)
  Math.sqrt( ( (x1-x2)**2 + (y1-y2)**2).abs )
end

def select_point(radius, points_list)
  result = []
  points_list.each do |point|
    dist = calc_distance( CONST_X, CONST_Y, point[0], point[1])
    result << point if dist <= radius
  end
  result
end

points = [[2,5],[1,1],[0,7],[0,6],[4,1],[2,3],[0,5]]
puts select_point(5, points)