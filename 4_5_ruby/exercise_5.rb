# Итак, это файл Ip_to_country в формате text/csv. Необходимо написать простую утилиту,
# на входе которой будет некий IP адрес, а на выходе код страны, к которой этот IP принадлежит.
# Программа должна использовать данный файл в качестве базы данных.
#
# Примечание: IP адреса зашифрованы в этом csv файле, будьте внимательны,
# подробности шифрования увидете в заголовке этого файла. Пример использования
# вашей утилиты может быть таким:
#
#$ iptocountry.rb 85.12.221.146
#RU

class IpToCountry

  def get_ip_country(ip_value)
    ip_decoded = encode_ip(ip_value)
    find_ip_in_file(ip_decoded) unless ip_decoded == nil
  end


  private
    def encode_ip(ip_value)
      ip_split = ip_value.split('.').reverse
      return if ip_split.size != 4

      ip_total = 0
      i = 0
      ip_split.each do |ip|
        ip_total += ip.to_i << i*8
        i += 1
      end
      ip_total
    end

  def decode_ip_record(ip_record)
    ip_record_split = ip_record.split(',')
    return if ip_record_split.size != 7

    ip_from = ip_record_split[0].delete("\"").to_i
    ip_to = ip_record_split[1].delete("\"").to_i

    ip_rec = {}
    ip_rec[:ip_range] = ip_from..ip_to
    ip_rec[:country] = ip_record_split[5]

    ip_rec
  end

  def find_ip_in_file(ip_encoded)
    File.open('IpToCountry.csv', 'r').each_line do |f_str|
      next if f_str[0] == '#'

      ip_rec = decode_ip_record(f_str)
      return ip_rec[:country] if ip_rec[:ip_range].include?(ip_encoded)
    end
  end
end


ip_finder = IpToCountry.new

if ARGF.argv.length > 0
  ARGF.argv.each {|param| puts "ip #{param} country #{ip_finder.get_ip_country(param)}"}
else
  puts ip_finder.get_ip_country('85.12.221.146')
  puts ip_finder.get_ip_country('49.128.178.210')
  puts ip_finder.get_ip_country('189.124.17.134')
  puts ip_finder.get_ip_country('183.221.160.204')
end






