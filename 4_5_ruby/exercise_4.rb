# Напишите программу, на входе которой некторое целое число, а на выходе его представление
# как на LCD дисплее калькулятора, например:

#$ digtolcd.rb 654
#   _   _
#  |_  |_  |_|
#  |_|  _|   |
#     _   _       _   _  __  _   _   _
# |   _|  _| |_| |_  |_   / |_| |_| | |
# |  |_   _|   |  _| |_| |  |_|  _| |_|

class ConverterToLcd
  def initialize
    @lcd_numbers = {}
    @lcd_numbers['1'] = ['  ',' |',' |']
    @lcd_numbers['2'] = [' _ ',' _|','|_ ']
    @lcd_numbers['3'] = [' _ ',' _|',' _|']
    @lcd_numbers['4'] = ['   ','|_|','  |']
    @lcd_numbers['5'] = [' _ ','|_ ',' _|']
    @lcd_numbers['6'] = [' _ ','|_ ','|_|']
    @lcd_numbers['7'] = ['__ ',' / ','|  ']
    @lcd_numbers['8'] = [' _ ','|_|','|_|']
    @lcd_numbers['9'] = [' _ ','|_|',' _|']
    @lcd_numbers['0'] = [' _ ','| |','|_|']
  end


  def convert_digit_to_lcd(value)
    result = ['','','']
    value.to_s.each_char do |char|
      next unless  @lcd_numbers.key? char

      lcd_value = @lcd_numbers[char]
      result[0] += lcd_value[0]
      result[1] += lcd_value[1]
      result[2] += lcd_value[2]
    end
    result
  end

  def print_lcd_digit(value)
    puts value[0]
    puts value[1]
    puts value[2]
  end

  def convert_and_print(value)
    dig = self.convert_digit_to_lcd(value)
    self.print_lcd_digit(dig)
  end
end

if ARGF.argv.length > 0
  ConverterToLcd.new.convert_and_print(ARGF.argv[0])
end