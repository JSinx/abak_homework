# Даны два списка (массива) из различных (возможно повторяющихся) целых чисел,
# а также могут быть и “дырки” - nil’ы, при этом каждый из массивов может в
# качестве элементов содержать подмассивы также из целых чисел. Пример:
# массив a = [[4, 19], nil, [32, 41], 65], b = [234, 0, 21, [54]].
#
# Необходимо написать программу на ruby, на выходе которой будет хеш,
# ключами которого будут числа из обоих массивов a и b,
# а соотв. значениями - сумма, сколько раз встречается число как в первом, так и во втором массиве.


list_1 =[[4, 19], nil, [32, 41], 65, 4, [4,19], 21, [nil, nil]]
list_2 = [234, 0, 21, [54]]

def calc_nums_count( list_a, list_b)
  total_array = list_a.flatten + list_b.flatten
  only_uniq = total_array.uniq
  only_uniq.compact!

  result = {}

  # only_uniq.each do |item_1|
  #   only_uniq.each do |item_2|
  #     result[[item_1, item_2]] = total_array.count(item_1) + total_array.count(item_2)
  #   end
  # end

  only_uniq.each do |item_1|
    result[item_1] = total_array.count(item_1)
  end
  result
end

puts calc_nums_count(list_1, list_2)


